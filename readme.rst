Extended Cubic Spline
=====================

Basis
-----

The cubic spline is a method than approximates a function known at some
points by a list of polynomials approximation. Typically, if we know the
values :math:`a_i` taken by a a function :math:`A(x)` at some points
:math:`x_i`, :math:`i=0, ..., N-1`. Between each points :math:`x_i` and
:math:`x_{i+1}`, we modelise :math:`A(x)` by the function

.. math:: A_i(x)=\frac{x-x_i}{x_{i+1}-x_i}a_{i+1}+\frac{x_{i+1}-x}{x_{i+1}-x_i}a_i+ \frac{1}{6}\left(\left(\frac{x-x_i}{x_{i+1}-x_i}\right)^3-\frac{x-x_i}{x_{i+ 1}-x_i}\right)\left(x_{i+1}-x_i\right)^2a_{i+1}''+\left(\left(\frac{x_{i+1}-x}{ x_{i+1}-x_i}\right)^3-\frac{x_{i+1}-x}{x_{i+1}-x_i}\right)\left(x_{i+1}-x_i \right)^2a_i''\label{cubicSplineBasic}

One can easily see that each of these approximations introduces unknows,
the second derivatives the the function: :math:`a_i''`. In the cubic
spline method, we fixed these unknowns by imposing a continuity on the
first derivative at each points :math:`x_i`. The continuity of the
function and the continuity of the second derivatives is assumed by the
shape of our approximations [cubicSplineBasic]. The continuity condition
is obtained by the equality of the first derivative of two neighborhood
approximation at the frontier. It is given by:

.. math:: \frac{x_i-x_{i-1}}{6}a_{i-1}''+\frac{x_{i+1}-x_{i-1}}{3}a_i''+\frac{x_{i+1}-x_i }{6}a_{i+1}''=\frac{a_{i+1}-a_i}{x_{i+1}-x_i}+\frac{a_{i-1}-a_i}{x_i-x_{i-1}} \label{basicContinuity}

The equation obtained is only valid for :math:`i\in{1, 2, ..., n-2}`.
For the first and the last element, there are different approaches to
close the system of equations. One have to consider his specific problem
and its condition at borders. But this is not of interest for us here,
since we want to developed an extended version of the cubic spline
method. Typically we choose a behavior at the border that enables us to
write the contiuity euqation as a linear system of equations
:math:`Da''=Va`, where :math:`D` and :math:`V` are two matrices.

In our case where the spline approximation will serve to compute many
integrals from :math:`-\infty` to :math:`+\infty`, it would be useful to
extend our spline to the whole domain of :math:`x`. We can consider our
approach as the combinaison of three different cubic splines, one that
covers the domain :math:`x\in(-\infty, -\omega_m)`, one that covers the
domain :math:`x\in[-\omega_m, \omega_p]` and the other for the domain
:math:`x\in(\omega_p, +\infty)`. expansion

The first thing to do consists in determining the variables we will use
in the additional domains that have an infinite length in :math:`x`. We
clearly need to use a variable that grows inversely to :math:`x`. In
order to not create artefact we need a variable :math:`y` such that

.. math:: \frac{dy}{dx}|_{x=\omega_p}=1,\qquad\frac{d^2y}{dx^2}|_{x=\omega_p} =0

The first condition implies that the variable y varies linearly with x
at the limit. The second condition implies that there is inflexion point
in the relation between both variables at the frontier. Since the second
derivative of the function appears in the expression of the cubic spline
the method will be more stable if we enforce both conditions. With those
condition we have

.. math:: \frac{df}{dy}|_{x=\omega_p}=\frac{dx}{dy}|_{x=\omega_p}\frac{df}{dx}|_{x=\omega_p}=\frac{df}{dx}|_{x=\omega_p}\frac{d^2f}{dy^2}|_{x=\omega_p}=\frac{dx}{dy}|_{x=\omega_p}(\frac{d}{dx}\frac{df}{dx})_{x=\omega_p}=\frac{d^2f}{dy^2}|_{x=\omega_p}

The simpliest expression that satisfies both conditions is

.. math:: y=\frac{-8\omega_p^3}{x^2+3\omega_p^2}

So, for the domain :math:`x\in[\omega_p,\infty)`, we’ll have
:math:`y\in[-2\omega_p,0)`. We can also fix the number of nodes in the
:math:`y` domain such that the step size is the same for both domains.
That way we ensure a better stability of the solution.

The specific choice made shows that the basic continuity equation
[basicContinuity] will have be appliable for every intervals between the
nodes. The continuity expression allows us to determine the second
derivative that appears in the spline expression in terms of the values.
But, we have two more unknowns than the number of conidtions. Usually,
one chooses a specific behavior of the first derivative at the first and
at the last nodes to close the system of equations. If our function is
symmetric, we can easily argue that its derivative at :math:`x=0` must
be :math:`0`. Concerning, the last node at :math:`x=\infty`, the value
is already fixed to :math:`0`. We fix the second derivative by imposing
the integrability of the function.

.. math:: \int_{x_N}^\infty dxf(x)=\int_{-\Delta}^0dy\frac{4\omega_p^2}{\Delta y\sqrt{ 8\omega_py-3y^2}}((y+\Delta)(f_\infty-f_\infty")-y(f_N-f_N")-y^3f_N"+(y+ \Delta)^3f_\infty)

where :math:`x_N=\omega_p\sqrt{8\omega_p/\Delta-3}`. For the equation to
be integrable, we need that f  " to be zero.
