#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 15:41:43 2012

@author: allenste
"""
import xml.dom.minidom as xml
import argparse
import numpy as np
import array
import string
import sys

class template(object) :
    """
    @brief this class construct a template class. It is used as a parent class
    for the other classes.
    @note : all the param elements are put as string format in the attributes.
    """
    ##
    # Basis parser for the class
    argparser = argparse.ArgumentParser(description="Basic parser")
    argparser.add_argument('-s', '--status', nargs=1, type=str, \
                           help='Computation status')


    def parseNumpyDict(self, npdict={}) :
        """
        @brief Set the attribute of the class from a dictionary of Numpy values.
        @param self: handle to the object
        @param npdict: dictionary with ndarray values (loaded from an npz file).
        """
        # If the dictionary is not passed in arguments is it defined as an attr
        if len(npdict) > 1 :
            dico = npdict
        elif hasattr(self, 'npdict') :
            self.npdict.update(npdict)
            dico = self.npdict
        else :
            dico = {}

        if 'param' in dico :
            # Build a list of string containing the attribute names and values
            l = array.array('B', dico.pop('param').tolist()).tostring().split()

            # Look for a dict of default values to find the type of the attr
            default = self.__dict__.copy()
            if hasattr(self,'default') and type(self.default).__name__=='dict' :
                default.update(self.default)

            # Parse the list of parameter attributes
            i = 0
            j = 0
            while (i < len(l)) :
                if l[i] == '<' :
                    # This attribute is an object
                    j = i + 1
                    while (j < len(l) and l[j] != ';') : j += 1
                    k = j + 1
                    d = {}
                    while (k < len(l) and l[k] != '>') :
                        if l[k] in dico : d[l[k]] = dico.pop(l[k])
                        k += 1
                    if j > i + 4 :
                        s = string.join(l[i+4:j])
                        d['param'] = np.array(array.array('b', s).tolist())
                    try :
                        exec "import " + l[i+2]
                    except :
                        print >>stderr, "Warning unable to load module "+l[i+2]
                    try :
                        exec "self."+l[i+1]+"="+l[i+2]+"."+l[i+3]+"(npdict=d)"
                    except :
                        print >>stderr, "Warning unable to build object "+l[i+3]
                    i = k + 1
                    j = 0

                elif i < len(l)-1 and l[i+1] == ':' :
                    # This attribute has a simple type
                    if l[i] in default :
                        typename = type(default[l[i]]).__name__
                        if hasattr(default[l[i]], "__module__") :
                            # if the default belongs to a module load it.
                            try :
                                exec "import" + default[l[i]].__module__
                                typename = default[l[i]].__module__+'.'+typename
                            except :
                                print >>stderr, "Unable to import a module"
                                continue
                        if typename == 'NoneType' : typename = 'str'
                    else : typename = 'str'

                    # Build the attribute
                    try :
                        exec 'self.' + l[i] + '=' + typename + '(l[i+2])'
                    except :
                        pass
                    i += 3
                    j = 0

                else :
                    # Assume that we are dealing with a list
                    if j == 0 :
                        j = i - 3
                        exec 'self.' + l[j] + " = [self." + l[j] + ",]"
                    else :
                        exec 'self.' + l[j] + ".append(" + typename + "(l[i]))"
                    i += 1

        # Put the ndarrays as attributes of the class.
        self.__dict__.update(dico)


    def loadFromNumpyFile(self, filename=None) :
        """
        @brief This function read the values of the attributes of the object
        from a file.
        @param self: handle to the object
        @param filename: name of the file from which you load the attributes.
        """
        if filename == None :
            # The default name is the name of the class + '.npz'
            filename = type(self).__name__ + '.npz'

        # Load the file
        try :
            dico = np.load(filename)
        except :
            if filename.lower().find('.npz') < 0 :
                # if the non-success is due to the absence of the extension
                # We do the check later to allow different type of extension.
                filename += '.npz'
                try :
                    dico = np.load(filename)
                except :
                    print >>sys.stderr, "Unable to load file:", filename
                    dico = {}
            else :
                dico = {}
                print >>sys.stderr, "Unable to load file:", filename

        # Parse the loaded data to build the attributes of the object.
        self.parseNumpyDict(dico)


    def parseArguments(self, argv) :
        """
        @brief This function parse the arguments to set specific attributes
        @param self: handle to the current object
        @param argv list of string of arguments to parse (like sys.argv).
        """
        if not hasattr(self, 'argparser') : return
        a = argv
        typename = type(a).__name__
        if typename == 'str' or typename == 'unicode' :
            # aruments: "-a 1 -b 3.1416"
            a = a.split()
            typename = 'list'
        if typename == 'list' :
            # arguments: ["a", "1", "b", "3.1416"]
            try :
                a = self.argparser.parse_args(a)
                typename = 'Namespace'
            except :
                if len(a) > 1 and type(a[0]).__name__ == 'tuple' :
                    # arguments: [("a","1"),("b","3.1416")]
                    try :
                        d = {}
                        for i in a : d[i[0]] = i[1]
                        a = d
                        typename = 'dict'
                    except :
                        typename = "NoneType"
        if typename == 'Namespace' :
            # arguments: /namespace/ a; b
            try :
                a = a.__dict__
                if 's' in a and not 'status' in a :
                    a['status'] = a.pop('s')
                typename = 'dict'
            except :
                typename = 'NoneType'
        if typename == 'dict' :
            self.__dict__.update(a)


    def getxmlValues(self, desc, **kwargs) :
        """
        @brief extract the parameter values from the parser result
        @param self: handle to the object
        @param desc: descendant of a tag name returned by the xml parser
        """
        dico = {}
        if 'dico' in kwargs and type(kwargs['dico']).__name__ == 'dict' :
            d = kwargs['dico']
        elif hasattr(self, 'default') and type(self.default).__name__=='dict' :
            d = self.default
        else : d = {}
        if 'tagname' in kwargs :
            select = True
            tag = kwargs[tagname]
        else : select = False
        if 'default' in kwargs and select :
            d[tag] = kwargs['default']
        try :
            children = desc[0].childNodes
        except :
            children = []
        for c in children :
            if hasattr(c, 'tagName') :
                s = c.tagName
                if select and s.find(tag) < 0 : continue
                entry = c.childNodes[0].data.replace(' ', '')
                v = entry
                if s in d :
                    typename = type(d[s]).__name__
                    if typename == 'NoneType' : typename = 'str'
                    elif typename == 'function' :
                        try :
                            typename = type(d[s](dico)).__name__
                        except :
                            typename = 'str'
                    elif hasattr(d[s], "__module__") and \
                         d[s].__module__!= '__main__':
                        m = d[s].__module__
                        try :
                            exec "import " + m
                            typename = m + "." + typename
                        except :
                            typename = 'str'
                else : typename = 'str'
                exec "dico[s] =" + typename + "(v)"
        if select : dico = dico.get(tag, d[tag])
        return dico


    def __init__(self, **kwargs) :
        """
        @brief Constructor for the template object
        @param self: handle to the current object
        @param kwargs: dictionary of arguments. Interpreted keys are
        * args: for commmand line arguments
        * paramfile: xml config parameter file. The tag must be the class name.
        * dico: dictionary containings the attributes
        """
        d = {}
        if hasattr(self, 'default') and type(self.default).__name__ == 'dict' :
            # Add the default attribute to the list of attributes
            d = self.default
        if 'dico' in kwargs and type(kwargs['dico']).__name__ == 'dict' :
            d.update(kwargs['dico'])
        d.update(self.__dict__)
        self.__dict__.update(d)

        # Parse the configuration file for new attribute values
        if 'paramfile' in kwargs : paramfile = kwargs['paramfile']
        else : paramfile = None
        if paramfile != None :
            try :
                dom = xml.parse(paramfile)
            except :
                if paramfile.lower.find('.xml') < 0 :
                    paramfile += '.xml'
                    try :
                        dom = xml.parse(paramfile)
                    except :
                        print >>sys.stderr,"Unable to parse the file ",paramfile
                else :
                    print >>sys.stderr,"Unable to parse the file ",paramfile
        else :
            # If no configuration file look for the file "param.xml"
            try :
                dom = xml.parse('param.xml')
            except :
                pass
        try :
            if 'tagname' in self.__dict__ : tagname = self.tagname
            else : tagname = type(self).__name__
            d.update(self.getxmlValues(dom.getElementsByTagName(tagname),dict=d))
        except :
            pass

        # Set the status of the compuation
        if "status" in kwargs : d['status'] = kwargs['status']
        else : d['status'] = 'new'

        # Set the attribute of the object
        self.__dict__.update(d)

        # Look for new attribute values in the command line
        if hasattr(self,"argparser") and "args" in kwargs :
            self.parseArguments(kwargs["args"])

        # if a filename in dictionary load the data
        if self.status.lower()=='continue' and not 'filename' in self.__dict__:
            if 'name' in self.__dict__ : self.filename = self.name+'.npz'
            else : self.filename = None
        if "filename" in self.__dict__ :
            self.loadFromNumpyFile(self.filename)

        # If a dictionary is pass to the constructor build the attribute from it
        dico = {}
        if "npdict" in self.__dict__ :
            dico = self.__dict__['npdict']
        if 'npdict' in kwargs :
            dico.update(kwargs['npdict'])
        if len(dico) > 0 : self.parseNumpyDict(dico)


    def getAsNumpyDict(self) :
        """
        @brief Generate and return a dictionary of ndarray from the attributes
        @param self: handle to the object
        @return dictionary of ndarrays
        """
        dico = self.__dict__.copy()
        s = ''
        # parse the list of attributes of the object
        for k, v in dico.items() :
            typename = type(v).__name__

            # the attribute value is an ndarray
            if typename == 'ndarray' :
                continue
            elif typename=='tuple' or typename=='list' or typename=='dict' :
                # The attribute is a dict, a list or a tuple
                dico[k] = np.array(v)
                continue

            if hasattr(v, "__module__") :
                # if the attribute value is an object
                try :
                    d = v.getAsNumpyDict()
                    s += "< "+k+" "+v.__module__+" "+type(v).__name__+" "
                    p = d.pop('param', None)
                    if p != None :
                        s += array.array('B', p.tolist()).tostring()
                    s += '; '
                    for k in d.keys() : s += k + ' '
                    s += '> '
                    dico.update(d)
                except :
                    # Ignore object that cannot be transfered to a numpy array
                    pass

            else :
                # The attribute value is a simple type
                s += k + " : " + str(v) + ' '

            # Remove the attribute put in param from the dictionary
            dico.pop(k)

        dico['param'] = np.array(array.array('b', s).tolist())
        return dico


    def saveInNumpyFile(self, filename=None) :
        """
        @brief Save the object in a npz file
        @param self: handle to the object
        @param filename
        """
        if filename == None : filename = type(self).__name__ + '.npz'
        elif filename.lower.find('.npz') < 0 : filename += '.npz'
        dico = self.getAsDict()
        np.savez(filename, **dico)
