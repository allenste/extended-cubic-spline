#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu May  1 15:41:43 2012

@author: allenste
"""
from template import template
import numpy as np
import math

class cubicspline(template) :
    """
    @brief This class build an object that build a cubic-spline representation
    of a function. That representation is build on an infinite or a
    semi-infinite domain, using a grid on x between -c and c (or 0 to c for the
    semi-infinite case) and a grid on 8c^3/(x^2+3c^2) from x to infinity.
    This specific function was chosen such that we have a continuity of
    the first and the second derivative at the cutoff c.
    """
    ##
    # Default values
    default = {'xcut' : 1., 'nxcut' : 101, 'symmetry' : True}


    def generateX(self) :
        """
        @brief This function generates the axis
        @param self: handle to the object
        """
        if self.symmetry :
            self.delta = self.xcut / (self.nxcut - 1)
            self.x = np.linspace(0., self.xcut, self.nxcut)
            nxh = 2 * self.nxcut - 2
            y = np.linspace(self.xcut+self.delta, 3*self.xcut, nxh)
            self.w = np.hstack((self.x, y))
            self.computex = lambda y : self.xcut*np.sqrt(8*self.xcut/(3* \
                self.xcut-y)-3.)
            self.computey = lambda x : 3*self.xcut-8*self.xcut/(3.+x*x \
                                                /self.xcut/self.xcut)
            self.x = np.hstack((self.x,self.computex(y[:-1])))
            self.ninterval = self.nxcut + nxh - 1
            self.change = (0, self.nxcut)
        else :
            self.delta = self.xcut * 2 / (self.nxcut - 1)
            self.x = np.linspace(-1*self.xcut, self.xcut, self.nxcut)
            nxh = self.nxcut - 1
            y = np.linspace(self.xcut+self.delta, 3*self.xcut, nxh)
            self.w = np.hstack((self.x, y))
            self.computex = lambda y : np.sign(y)*self.xcut*np.sqrt(8*self.xcut\
                /(3*self.xcut-np.absolute(y))-3.)
            self.computey = lambda x : np.sign(x)*(3*self.xcut-8*self.xcut/(3. \
                                                +x*x/self.xcut/self.xcut))
            self.x =np.hstack((self.x,self.computex(y[:-1])))
            y = np.linspace(-3*self.xcut, -1*self.xcut-self.delta, nxh)
            self.x = np.hstack((self.computex(y[1:]), self.x))
            self.w = np.hstack((y, self.w))
            self.ninterval = 2 * nxh + self.nxcut - 1
            self.change = (nxh, nxh+self.nxcut)
        self.nnodes = self.ninterval + 1
        self.nxh = nxh


    def generateMatrix(self) :
        """
        @brief This function generates the matrix linking the variables of
        the cubic spline representation to the value of the function.
        @param self: handle to the current object
        """
        # build the matrix f1, f2 for the polynomial representation of the
        # cubic spline : f(x) = f1(0)*v + f1(1)*x*v + f(2)*x^2*v + f(3)*x^3*v
        # + f2(0)*v" + f2(1)*v"*x + f2(2)*v"*x*x + f2(3)*v"*x^3
        # Same expression for both domain, we'll have to pay attention when
        # linking the v" to the v.
        #f1 = np.zeros((self.ninterval, 4, self.nnodes), dtype=np.float64)
        #f2 = np.zeros((self.ninterval, 4, self.nnodes), dtype=np.float64)
        #for i in range(self.ninterval) :
        #    f1[i,1,i] = 1.
        #    f1[i,0,i+1] = 1.
        #    f1[i,1,i+1] = -1.
        #    f2[i,1,i] = -1. / 6.
        #    f2[i,3,i] = 1. / 6.
        #    f2[i,1,i+1] = -1. / 3.
        #    f2[i,2,i+1] = 0.5
        #    f2[i,3,i+1] = -1. / 6.

        # Build the continuation condition of the cubic spline: d1 * v = d2 * v"
        # The constance of the steps simplifies the equations
        d1 = np.zeros((self.nnodes, self.nnodes), dtype=np.float64)
        d2 = np.zeros((self.nnodes, self.nnodes), dtype=np.float64)
        i = np.arange(self.nnodes-1, dtype=np.int32)
        if self.symmetry :
            d1[0,0] = -1.
            d1[0,1] = 1.
            d2[0,1] = 1. / 6.
        else : d1[0,0] = 1. / 3.
        d1[i[1:],i[1:]] = -2.
        d1[-1,-1] = 1. / 3.
        d1[i[1:-1],i[:-2]] = 1.
        d1[i[1:-1],i[2:]] = 1.
        d2[0,0] = 1. / 3.
        d2[i[1:],i[1:]] = 2. / 3.
        d2[-1,-1] = 1. / 3.
        d2[i[1:-1],i[2:]] = 1. / 6.
        d2[i[1:-1],i[:-2]] = 1. / 6.
        self.matrix = np.dot(np.linalg.inv(d2), d1) / 6.


    def __init__(self, **kwargs) :
        """
        @brief This function builds an object of cubic-spline type
        @param self: handle to the object
        @param kwargs: List of parameter to set the cubic-spline
        """
        # Call the parent builder to parse file and arguments
        template.__init__(self, **kwargs)

        # make sure values have the right type
        self.nxcut = int(self.nxcut)
        self.xcut = float(self.xcut)

        # Build the axis
        self.generateX()

        # Generate the matrix (M) linking the variables to the function
        # such that: f(x) = M * v
        self.generateMatrix()


    def toMixedAxis(self, x) :
        """
        @brief this function returns a list of points in representation by
        the list of previous nodes and the distance to that node. That distance
        is evaluated in a mixed array framework (see notes).
        @param self: handle to the current object
        @param x: ndarray list of points in entry and distance to previous node
        in exit.
        @return ndarray of index of previous nodes
        """
        i = np.ndarray(x.shape, dtype=np.int64)
        if self.symmetry :
            j = np.where(x <= self.xcut)
            k = np.where(x > self.xcut)
            i[j] = np.int_(x[j] / self.delta)
            x[k] = self.computey(x[k])
            i[k] = np.int_((x[k]-self.xcut)/self.delta) + self.nxcut
        else :
            j = np.where(x >= -1*self.xcut & x <= self.xcut)
            k = np.where(x < -1*self.xcut)
            l = np.where(x > self.xcut)
            i[j] = np.int_((x[j]+self.xcut)/self.delta) + self.nxh
            x[k] = self.computey(x[k])
            i[k] = np.int_((x[k]-self.w[0]) / self.delta)
            x[l] = self.computey(x[k])
            i[l] = np.int_((x[k]-self.xcut)/self.delta)+self.nxcut+self.nxh
        return i


    def __call__(self, x) :
        """
        @brief compute the function for a given x
        @param self: handle to the current object
        @param x: point where we want to evaluate the function
        @return the value of the function
        """
        if not hasattr(self, 'var') :
            print >>sys.stderr, "Error: The variable values are not set"
            return None
        if type(x).__name__ == 'ndarray' : y = x.copy()
        else : y = np.array(x)
        i = self.toMixedAxis(y)
        l = [4,]
        l.extend(y.shape)
        w = np.ndarray(tuple(l), dtype=np.float64)
        w[0,:] = (y - self.w[i]) / self.delta
        w[1,:] = 1. - w[0,:]
        w[2,:] = np.power(w[0,:], 3) - w[0,:]
        w[3,:] = np.power(w[1,:], 3) - w[1,:]
        ddy = np.dot(self.matrix, self.var)
        r =w[0,:]*self.var[i+1]+w[1,:]*self.var[i]+w[2,:]*ddy[i+1]+w[3,:]*ddy[i]
        return r


    def setvar(self, v) :
        """
        @brief this function sets the values of the variables
        @param self: handle to the current object
        @param v: ndarray or variables object
        @note the attribute var contains the value at -oo and +oo. Generally,
        these are zero. We put them in, to simplify some expressions.
        """
        if not type(v).__name__ == 'ndarray' :
            try :
                var = v.getValues()
            except :
                print >>sys.stderr, "Error: Unrecognized type of varaibles"
        else : var = v
        if var.size < self.nnodes :
            if self.symmetry : self.var = np.array(var.tolist()+[0.,])
            else : self.var = np.array([0.,]+var.tolist()+[0.,])
        else : self.var = var


    def getvar(self) :
        """
        @brief This function return the variables
        @param self: handle to the current object
        @return ndarray containing the variables
        """
        if not 'var' in self.__dict__ :
            self.var = np.zeros((self.nnodes,), dtype=np.float64);
        return self.var

    def getvarsize(self) :
        """
        @brief This function returns the size of the variables
        @param self: handle to the current object
        @return the number of variables
        """
        return self.nnodes


    def generate_integral(self, fct=None, varsymbol='x') :
        """
        @brief This function generates the integral of a function involving the
        spline. It returns the result as a vector by which we need to multiply
        the spline to compute the integral for a given spline.
        @param self: handle to the current object
        @param fct: function that we want to integrate with the spline
        @param varsymbol: character representing the variable in the function
        @return an ndarray contianing the integration matrix
        """
        I = np.zeros((self.nnodes,), dtype=np.float64)
        Ipp = np.zeros((self.nnodes,), dtype=np.float64)
        if fct == None :
            # integrate the spline only
            xc = 2*self.xcut
            cst = xc*xc/self.delta/math.sqrt(3.)
            cst2 = xc / self.delta / 3.
            if not self.symmetry :
                # Integration from -infinity to the cutoff.
                end = self.change[0] + 1
                y = np.arange(end,dtype=np.float64)*self.delta*1.5/xc
                R = np.sqrt((2.-y)*y)
                asx = np.arcsin(1. - y)
                I[1:end] = cst*(asx[:-2]-2*asx[1:-1]+asx[2:]-R[:-2]+2*R[1:-1] \
                        -R[2:])
                a = cst2*cst2*((2*y+5.)*y/3.-1.)-1./6.
                b = cst2*cst2*((1.-y)*2*y-1.)+1./6.
                Ipp[1:end] = cst*(-1*a[:-2]*R[:-2]+2*(a[1:-1]+0.5)*R[1:-1] \
                    -a[2:]*R[2:]+b[:-2]*(asx[1:-1]-asx[:-2])+b[2:]*(asx[1:-1]- \
                    asx[2:]))
                I[end] = cst*(asx[-2]-asx[-1]-R[-2]+(y[-2]/y[-1])*R[-1])
                Ipp[end] = cst*((a[-1]+0.5-cst2*(1.+y[-1]))*R[-1]-a[-2]* \
                    R[-2]+b[-2]*(asx[-1]-asx[-2]))

            # integration over the intervals with regular spacing
            I[self.change[0]] += 0.5 * self.delta
            Ipp[self.change[0]] += -0.25 * self.delta / 6.
            i = np.arange(self.change[0]+1, self.change[1]-1, dtype=np.int32)
            I[i] = self.delta
            Ipp[i] = self.delta * -0.5 / 6.
            I[self.change[1]-1] = 0.5 * self.delta
            Ipp[self.change[1]-1] = -0.25 * self.delta / 6.

            # integration from the last cutoff to infinity
            beg = self.change[1] - 1
            end = self.nnodes
            y = np.arange(end-beg-1,-1,-1,dtype=np.float64)*self.delta*1.5/xc
            R = np.sqrt((2.-y)*y)
            asx = np.arcsin(1. - y)
            I[beg] += cst*(asx[1]-asx[0]-R[1]+(y[1]/y[0])*R[0])
            a = cst2*cst2*((2*y+5.)*y/3.-1.)-1./6.
            b = cst2*cst2*((1.-y)*2*y-1.)+1./6.
            Ipp[beg] += cst*((a[0]+0.5-cst2*(1.+y[0]))*R[0]-a[1]*R[1]+b[1]*( \
                asx[0]-asx[1]))
            I[beg+1:end-1] = cst*(asx[:-2]-2*asx[1:-1]+asx[2:]-R[:-2]+2* \
                                  R[1:-1]-R[2:])
            Ipp[beg+1:end-1] = cst*(-1*a[:-2]*R[:-2]+2*(a[1:-1]+0.5)*R[1:-1] \
                -a[2:]*R[2:]+b[:-2]*(asx[1:-1]-asx[:-2])+b[2:]*(asx[1:-1]- \
                asx[2:]))

            # Final result
            I += np.dot(Ipp, self.matrix) / 6.
            if self.symmetry : I *= 2
            self.Integrate = I

        else :
            # Integrate with a given function
            print "This function is not build yet."
            I = None

        return I


    def integrate(self, fct=None, varsymvol='x', splinesymbol='s') :
        """
        @brief Computes the integral of a function involving the spline.
        @param self: handle to the current object
        @param fct: function to integrate
        @param varsymbol: character representing the variable in the function
        @param splinesymbol: character representing the spline in the function
        @return a float giving the value of the integral
        """
        if fct == None :
            if not hasattr(self, 'Integrate') :
                self.generate_integral()
            I = self.Integrate
        else :
            expr = fct / splinesymbol
            if splinesymbol in expr.free_symbols :
                # Not simply a product of a function by the spline
                if hasattr(self, 'var') :
                    # Compute the dependant parameters in the cubic spline
                    fdd = np.dot(self.matrix, self.var)
                    # Compute the integral
                else :
                    print >>sys.stderr,"Error: variables not set to compute int"
            else :
                I = self.generate_integral(expr, varsymbol, splinesymbol)
        if hasattr(self, 'var') and type(I).__name__ == 'ndarray' :
            I = np.dot(I, self.var)
        return I
